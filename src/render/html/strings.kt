/*
 * WeightSiteGen
 * Copyright (C) 2019  James P. Mitchell
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package render.html

import WeightRecord
import WeightRecords
import analysis.*
import com.samskivert.mustache.Mustache
import getResourceFileText
import org.joda.time.DateTime
import kotlin.math.roundToInt

class DateRangeAnalysisStrings(dra: DateRangeAnalysis) {
    val key         = dra.key
    val name        = dra.name
    val numRecords  = dra.numRecords.toHTML()
    val weightRange = "${dra.first.weight.toShortHTML()} - ${dra.last.weight.toShortHTML()}"
    val delta       = dra.delta.toShortHTML()
    val mean        = dra.mean.toShortHTML()
    val mad         = dra.mad.toLongHTML()
    val s           = dra.ser.toLongHTML()
}

class WeightRecordString(wr: WeightRecord) {
    val weight = wr.weight.toShortHTML()
    val date = wr.date.toShortHTML()
    val comment = wr.comment
}

class WeightRecordsStrings(weightRecords: WeightRecords) {
    val values = weightRecords.values.map { WeightRecordString(it) }.reversed()
    val hasComments = values.any { it.comment.isNotEmpty() }
}

class MilestoneStrings(milestone: Milestone, val type: String, val description: String) {
    val status = milestone.status
    val isWanted = milestone.isWanted
    val weight = milestone.weight.toShortHTML()
    val delta = milestone.delta.toShortHTML()
    val date = when (milestone.status) {
        MilestoneStatus.Lost,
        MilestoneStatus.Gained -> milestone.date!!.toShortHTML()
        MilestoneStatus.NeverBelow -> if (milestone.type != MilestoneType.Underweight) "Not Yet" else "n/a"
        MilestoneStatus.NeverAbove -> "n/a"
    }

    val period: String = when(milestone.status) {
        MilestoneStatus.Lost -> "Not Maintained"
        MilestoneStatus.Gained -> milestone.period!!.toHTML()
        MilestoneStatus.NeverBelow,
        MilestoneStatus.NeverAbove -> "n/a"
    }
}

class StringVars(
    height: Double,
    wrist: Double,
    val gender: String,
    val version: String
) {
    val height = height.run {
        val iHeight = height.roundToInt()
        "${iHeight / 12}ft ${iHeight % 12}in"
    }

    val wrist = "${wrist}in"

    val date = DateTime.now().toLongHTML()

    val varMap = mapOf(
        Pair("height"   , this.height),
        Pair("wrist"    , this.wrist),
        Pair("gender"   , this.gender),
        Pair("version"  , this.version),
        Pair("date"     , this.date)
    )
}

class MilestonesStrings(private val stringVars: StringVars, milestones: Milestones) {
    val values = listOfNotNull(
        ms(milestones.obese3        , "Obesity Class III"   , "www/html/obese3.html"),
        ms(milestones.obese2        , "Obesity Class II"    , "www/html/obese2.html"),
        ms(milestones.obese1        , "Obesity Class I"     , "www/html/obese1.html"),
        ms(milestones.overweight    , "Overweight"          , "www/html/overweight.html"),
        ms(milestones.hamwi         , "Hamwi Formula"       , "www/html/hamwi.html"),
        ms(milestones.devine        , "Devine Formula"      , "www/html/devine.html"),
        ms(milestones.robinson      , "Robinson Formula"    , "www/html/robinson.html"),
        ms(milestones.miller        , "Miller Formula"      , "www/html/miller.html"),
        ms(milestones.lemmens       , "Lemmens Formula"     , "www/html/lemmens.html"),
        ms(milestones.broca         , "Broca Formula"       , "www/html/broca.html"),
        ms(milestones.underweight   , "Underweight"         , "www/html/underweight.html")
    ).sortedByDescending { it.first }.map { it.second }

    private fun ms(milestone: Milestone?, type: String, descriptionPath: String) =
        if (milestone == null)
            null
        else
            Pair(
                milestone.weight,
                MilestoneStrings(milestone, type, Mustache.compiler().compile(getResourceFileText(descriptionPath)).execute(stringVars.varMap))
            )
}

class LatestRecordingStrings(sw: StandardWeights, latest: WeightRecord) {
    val date = latest.date.toShortHTML()
    val weight = "${latest.weight.toShortHTML()} lbs"
    val bmi = "BMI ${sw.bmi(latest.weight).toShortHTML()}"
}

fun getForeword(stringVars: StringVars): String = Mustache.compiler().compile(getResourceFileText("www/html/foreword.html")).execute(stringVars.varMap)
fun getEpilogue(stringVars: StringVars): String = Mustache.compiler().compile(getResourceFileText("www/html/epilogue.html")).execute(stringVars.varMap)