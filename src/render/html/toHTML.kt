/*
 * WeightSiteGen
 * Copyright (C) 2019  James P. Mitchell
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package render.html

import org.joda.time.DateTime
import org.joda.time.Period
import org.joda.time.format.DateTimeFormat
import org.joda.time.format.PeriodFormatterBuilder
import java.text.DecimalFormat

private val FORMAT_DISPLAY_DEC_SHORT = DecimalFormat("0.0")
private val FORMAT_DISPLAY_DEC_LONG = DecimalFormat("0.000")
private val FORMAT_DISPLAY_DATE_SHORT = DateTimeFormat.forPattern("MMM dd, YYYY")
private val FORMAT_DISPLAY_DATE_LONG = DateTimeFormat.forPattern("EEEE MMMM d, YYYY")
private val FORMAT_PERIOD = PeriodFormatterBuilder()
    .appendYears()
    .appendSuffix(" year", " years")
    .appendSeparator(", ")
    .appendMonths()
    .appendSuffix(" month", " months")
    .appendSeparator(", ")
    .appendDays()
    .appendSuffix(" day", " days")
    .toFormatter()

fun Double.toShortHTML(): String = FORMAT_DISPLAY_DEC_SHORT.format(this)
fun Double.toLongHTML(): String = FORMAT_DISPLAY_DEC_LONG.format(this)
fun Int.toHTML(): String = this.toString()
fun DateTime.toShortHTML(): String = FORMAT_DISPLAY_DATE_SHORT.print(this)
fun DateTime.toLongHTML(): String = FORMAT_DISPLAY_DATE_LONG.print(this)
fun Period.toHTML(): String = FORMAT_PERIOD.print(this)
fun String.orEmpty(b: Boolean) = if (b) this else ""