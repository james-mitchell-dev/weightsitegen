/*
 * WeightSiteGen
 * Copyright (C) 2019  James P. Mitchell
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package render.html

import analysis.MilestoneStatus
import getResourceFileText
import kotlinx.html.*
import kotlinx.html.stream.appendHTML
import render.js.toJS

fun DIV.navTab(id: String, selected: Boolean = false, block : DIV.() -> Unit) =
    div("tab-pane fade " + "show active".orEmpty(selected)) {
        this.id = id
        block()
    }

fun UL.navItem(id:String, selected:Boolean = false, block : A.() -> Unit) =
    li("nav-item") {
        a (classes = "nav-link " + "active".orEmpty(selected) ) {
            attributes["data-toggle"] = "tab"
            attributes["href"] = "#$id"
            attributes["role"] = "tab"
            attributes["aria-selected"] = selected.toJS()
            block()
        }
    }

fun DIV.navTable(centeredCells: Boolean = true, block : TABLE.() -> Unit) =
    div ("table-responsive") {
        table ("table table-borderless table-sm ${"table-centered-cells".orEmpty(centeredCells)} m-0 text-nowrap") {
            block()
        }
    }

fun TBODY.dateRangeAnalysisRow(color: String, dra: DateRangeAnalysisStrings) =
    tr {
        th(ThScope.row, classes = "text-$color") { +dra.name }
        td { +(dra.weightRange)    }
        td { +(dra.delta)          }
        td { +(dra.mean)           }
        td { +(dra.mad)            }
        td { +(dra.s)              }
    }

fun TBODY.milestoneRow(m: MilestoneStrings) =
    tr {
        th(ThScope.row) {
            a { setAsPopover(m.type, m.description) }
        }
        td { +m.weight }
        td { +m.delta }
        td {
            classes = when (m.status) {
                MilestoneStatus.Lost -> if (m.isWanted) setOf("text-danger") else setOf()
                MilestoneStatus.Gained -> setOf()
                MilestoneStatus.NeverBelow -> setOf("text-muted")
                MilestoneStatus.NeverAbove -> setOf("text-success")
            }
            +m.date
        }
        td {
            classes = when(m.status) {
                MilestoneStatus.Lost -> if (m.isWanted) setOf("text-right", "text-danger") else setOf("text-right")
                MilestoneStatus.Gained -> setOf("text-right")
                MilestoneStatus.NeverBelow -> setOf("text-right", "text-muted")
                MilestoneStatus.NeverAbove -> setOf("text-right", "text-success")
            }

            +m.period
        }
    }

fun A.setAsPopover(label: String, content: String) {
    attributes["id"] = "aPopover"
    attributes["class"] = "text-info"
    attributes["style"] = "text-decoration: none !important; cursor: help;"
    attributes["tabindex"] = "0"
    attributes["data-toggle"] = "popover"
    attributes["data-trigger"] = "focus"
    attributes["data-boundary"] = "window"
    attributes["data-placement"] = "bottom"
    attributes["data-html"] = "true"
    attributes["data-content"] = content
    +label
}


fun DIV.chartCheckbox(id: String, label: String, color:String, checked: Boolean) =
    div ("custom-control custom-checkbox custom-control-inline") {
        input(InputType.checkBox, classes = "custom-control-input") {
            this.autoComplete=false
            this.checked = checked
            this.id = id
            this.name = id
        }
        label ("custom-control-label") {
            attributes["for"] = id
            style = "color: var(--${color});"
            +label
        }
    }

fun DIV.chartDateRangeButton(dra: DateRangeAnalysisStrings, color:String) =
    div {
        button (type=ButtonType.button, classes="btn btn-outline-$color btn-sm text-nowrap mr-1") {
            id="btnDateRange"
            attributes["data-analysis"] = dra.key
            +dra.name
        }
    }

fun renderHTML(
    compiledCss             : String,
    compiledJs              : String,
    foreword                : String,
    epilogue                : String,
    latestStrings           : LatestRecordingStrings,
    milestoneStrings        : MilestonesStrings,
    weightRecordStrings     : WeightRecordsStrings,
    allAnalysisStrings      : List<DateRangeAnalysisStrings>,
    lastAnalysisStrings     : List<DateRangeAnalysisStrings>,
    yearlyAnalysisStrings   : List<DateRangeAnalysisStrings>) =
    buildString {
    appendln("<!DOCTYPE html>")
    appendHTML().
        html {
            head {
                meta (charset = "utf-8")
                style {
                    unsafe {
                        +"\n$compiledCss\n    "
                    }
                }
                script {
                    unsafe {
                        +"\n$compiledJs\n    "
                    }
                }
                title("Weight Recordings")
            }
            body("container") {
                div ("row pt-2") {
                    div ("col") {
                        p {
                            span ("text-muted") { +"Latest Recording:" }
                            +" "
                            span ("badge badge-primary align-text-bottom") { +latestStrings.date }
                            +" "
                            span ("badge badge-primary align-text-bottom") { +latestStrings.weight }
                            +" "
                            span ("badge badge-primary align-text-bottom") { +latestStrings.bmi }
                        }

                        //unsafe { +getResourceFileText("www/html/foreword.html").format(miscStrings.heightString, miscStrings.genderString) }
                        unsafe { +foreword }

                        chartCheckbox("cboxTrendLine", "Trend Line", "red", true)
                        chartCheckbox("cboxMeanLine", "Mean Line", "teal", false)
                        chartCheckbox("cboxTooltip", "Tooltip", "light", false)
                    }
                }

                div ("row pt-2") {
                    div ("col") {
                        div ("card") {
                            div ("card-body") {
                                div ("d-flex flex-column") {

                                    small("text-muted text-nowrap") { +"Date Ranges" }
                                    div ("d-flex flex-row flex-nowrap overflow-auto p-1") {
                                        allAnalysisStrings.forEach { chartDateRangeButton(it, "light") }
                                        lastAnalysisStrings.forEach { chartDateRangeButton(it, "danger") }
                                        yearlyAnalysisStrings.forEach { chartDateRangeButton(it, "warning") }
                                    }

                                    div {
                                        style = "width: 100%; padding-top: 56.25%; position: relative;"
                                        div {
                                            id="chart"
                                            style="position: absolute; top:0; bottom:0; left:0; right:0;"
                                        }
                                    }

                                    small("text-muted text-nowrap") { +"Analysis" }
                                    div ("d-flex flex-row flex-nowrap overflow-auto text-nowrap") {
                                        style = "scroll-snap-type: x mandatory;"

                                        div ("m-1 pr-2 border-right border-secondary text-nowrap") {
                                            style = "scroll-snap-align: start;"
                                            span {id="selFirstDate"}
                                            +" - "
                                            span {id="selLastDate"}
                                            p ("m-0") {
                                                small ("text-muted") {
                                                    +"Date Range "
                                                    b {
                                                        +"("
                                                        span {id="selDateRange"}
                                                        +")"
                                                    }
                                                }
                                            }
                                        }

                                        div ("m-1 pr-2 border-right border-secondary text-nowrap") {
                                            style = "scroll-snap-align: start;"
                                            span {id="selFirstRecord"}
                                            +" lbs - "
                                            span {id="selLastRecord"}
                                            +" lbs"
                                            p ("m-0") {
                                                small ("text-muted") {
                                                    +"Weight Range "
                                                    b {
                                                        +"("
                                                        span {id="selRecordRange"}
                                                        +")"
                                                    }
                                                }
                                            }
                                        }

                                        div ("m-1 pr-2 border-right border-secondary text-nowrap") {
                                            style = "scroll-snap-align: start;"
                                            span {id="selDelta"}
                                            +" lbs"
                                            p ("m-0") {
                                                small ("text-muted") {
                                                    +"Delta "
                                                    b {
                                                        +"("
                                                        span {id="selPercentChange"}
                                                        +")"
                                                    }
                                                }
                                            }
                                        }

                                        div ("m-1 pr-2 border-right border-secondary text-nowrap") {
                                            style = "scroll-snap-align: start;"
                                            span {id="selMean"}
                                            +" lbs"
                                            p ("m-0") {
                                                small ("text-muted") { +"Mean" }
                                            }
                                        }

                                        div ("m-1 pr-2 border-right border-secondary text-nowrap") {
                                            style = "scroll-snap-align: start;"
                                            span{id="selMad"}
                                            p ("m-0") {
                                                small {
                                                    a {
                                                        setAsPopover("MAD", getResourceFileText("www/html/mad.html"))
                                                    }
                                                }
                                            }
                                        }

                                        div ("m-1 pr-2 text-nowrap") {
                                            style = "scroll-snap-align: end;"
                                            span{id="selSER"}
                                            p ("m-0") {
                                                small {
                                                    a {
                                                        setAsPopover("SER", getResourceFileText("www/html/ser.html"))
                                                    }
                                                }
                                            }
                                        }
                                    }

                                }
                            }
                        }
                    }
                }

                div ("row pt-2") {
                    div ("col") {
                        ul ("nav nav-pills nav-fill") {
                            navItem("tab1", true) { +"Milestones" }
                            navItem("tab2", false) { +"Date Range Analysis" }
                            navItem("tab3", false) {
                                +"Raw Data "
                                span ("badge badge-pill badge-primary align-text-bottom") { +weightRecordStrings.values.size.toString() }
                            }
                        }

                        div ("card mt-2") {
                            div ("card-body") {
                                div ("tab-content") {
                                    navTab("tab1", true) {
                                        if (milestoneStrings.values.isEmpty()) {
                                            h4("text-muted text-centered"){
                                                +"No Milestones Available"
                                            }
                                        }
                                        else {
                                            navTable {
                                                thead {
                                                    tr {
                                                        th (ThScope.col) {  }
                                                        th (ThScope.col) { +"Weight" }
                                                        th (ThScope.col) { +"Delta" }
                                                        th (ThScope.col) { +"Date Achieved" }
                                                        th (ThScope.col, classes = "text-right") { +"Maintenance Period" }
                                                    }
                                                }

                                                tbody ("tbody-text-center") {
                                                    milestoneStrings.values.forEach {
                                                        milestoneRow(it)
                                                    }
                                                }
                                            }
                                        }
                                    }

                                    navTab("tab2") {
                                        navTable {
                                            thead {
                                                tr {
                                                    th(ThScope.col) { }
                                                    th(ThScope.col) { +"Weight Range" }
                                                    th(ThScope.col) { +"Delta" }
                                                    th(ThScope.col) { +"Mean" }
                                                    th(ThScope.col) { +"MAD" }
                                                    th(ThScope.col) { +"SER" }
                                                }
                                            }

                                            tbody {
                                                allAnalysisStrings.forEach { dateRangeAnalysisRow("light", it) }
                                                lastAnalysisStrings.forEach { dateRangeAnalysisRow("danger", it) }
                                                yearlyAnalysisStrings.forEach { dateRangeAnalysisRow("warning", it) }
                                            }
                                        }
                                    }

                                    navTab("tab3") {
                                        navTable (false) {
                                            thead {
                                                tr {
                                                    th(ThScope.col) { +"Date" }
                                                    th(ThScope.col) { +"Weight" }

                                                    if (weightRecordStrings.hasComments) {
                                                        th(ThScope.col) { +"Comment" }
                                                    }
                                                }
                                            }

                                            tbody {
                                                if (weightRecordStrings.hasComments) {
                                                    weightRecordStrings.values.forEach {
                                                        tr {
                                                            td { +it.date }
                                                            td { +it.weight }
                                                            td { +it.comment }
                                                        }
                                                    }
                                                }
                                                else {
                                                    weightRecordStrings.values.forEach {
                                                        tr {
                                                            td { +it.date }
                                                            td { +it.weight }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }

                div ("row py-2") {
                    div ("col") {
                        unsafe { +epilogue }
                    }
                }
            }
        }
    appendln()
}