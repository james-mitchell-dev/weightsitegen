/*
 * WeightSiteGen
 * Copyright (C) 2019  James P. Mitchell
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package render.js

import analysis.DateRangeAnalysis
import WeightRecords
import analysis.WeekDayIndices

fun renderWeekDayIndicesJS(wdi: WeekDayIndices) =
    StringBuilder().apply {
        append("{")
        append(" mon: ${wdi.monday.toJS()},")
        append(" tue: ${wdi.tuesday.toJS()},")
        append(" wed: ${wdi.wednesday.toJS()},")
        append(" thu: ${wdi.thursday.toJS()},")
        append(" fri: ${wdi.friday.toJS()},")
        append(" sat: ${wdi.saturday.toJS()},")
        append(" sun: ${wdi.sunday.toJS()}")
        append(" }")
    }.toString()

fun renderDateRangeAnalysisJS(dra: DateRangeAnalysis) =
    StringBuilder().apply {
        append("{")
        append(" mean : ${dra.mean.toJS()},")
        append(" slope : ${dra.slope.toJS()},")
        append(" intercept : ${dra.intercept.toJS()},")
        append(" ser : ${dra.ser.toJS()},")
        append(" mad : ${dra.mad.toJS()},")
        append(" delta : ${dra.delta.toJS()},")
        append(" percentChange : ${dra.percentChanged.toJS()},")
        append(" weightRange: { ")
        append(" min: ${dra.weightRange.min.toJS()},")
        append(" max: ${dra.weightRange.max.toJS()}")
        append(" },")
        append(" first : {")
        append(" index : ${dra.first.index.toJS()},")
        append(" date : ${dra.first.date.toJS()},")
        append(" weight : ${dra.first.weight.toJS()},")
        append(" trend : ${dra.first.trend.toJS()}")
        append(" },")
        append(" last : {")
        append(" index : ${dra.last.index.toJS()},")
        append(" date : ${dra.last.date.toJS()},")
        append(" weight : ${dra.last.weight.toJS()},")
        append(" trend : ${dra.last.trend.toJS()},")
        append(" weekDayIndices: ${renderWeekDayIndicesJS(dra.wdi)}")
        append(" },")
        append(" numRecords : ${dra.numRecords.toJS()}")
        append(" }")
    }.toString()

fun renderWeightSiteGenJS(records: WeightRecords, dateRangeAnalysis: List<DateRangeAnalysis>) =
    StringBuilder().apply {
        append(""" 
                    |"use strict"; 
                    |const wsg = {
                    |   data: [""".trimMargin())
        records.values.forEach{ append("[new Date(${it.date.millis}),${it.weight}],") }
        appendln("],")
        appendln("    analyses: {")
        dateRangeAnalysis.forEach { appendln("        ${it.key.padEnd(5)} : ${renderDateRangeAnalysisJS(it)},") }
        appendln("    },")
        append("""
                    |    getDate: (d) => d[0],
                    |    getWeight: (d) => d[1],
                    |    createAnalysis: function (records, firstIndex, lastIndex) {
                    |        function calculateStatistics(data, x, y) {
                    |            function pow2(x) { return x*x }
                    |        
                    |            const len   = data.length
                    |            const xMean = data.reduce((sum, d) => sum + x(d), 0) / len 
                    |            const yMean = data.reduce((sum, d) => sum + y(d), 0) / len
                    |        
                    |            const xxBar = data.reduce((sum, d) => sum + pow2(x(d) - xMean), 0)
                    |            const xyBar = data.reduce((sum, d) => sum + ( (x(d) - xMean) * (y(d) - yMean) ), 0.0)
                    |        
                    |            const slope     = xyBar / xxBar 
                    |            const intercept = yMean - slope * xMean 
                    |            const ser       = data.reduce((sum, d) => sum + Math.abs(slope * x(d) + intercept - y(d)), 0) / len
                    |            const mad       = data.reduce((sum, d) => sum + Math.abs(y(d) - yMean), 0) / len
                    |        
                    |            return { 
                    |                mean      : yMean, 
                    |                slope     : slope, 
                    |                intercept : intercept, 
                    |                ser       : ser, 
                    |                mad       : mad,
                    |                predict   : function(xValue) {
                    |                    return x(xValue) * slope + intercept
                    |                }
                    |            }
                    |        }
                    |        
                    |        const firstRecord      = records[0]
                    |        const lastRecord       = records[records.length-1]
                    |        const firstWeight      = wsg.getWeight(firstRecord)
                    |        const lastWeight       = wsg.getWeight(lastRecord)
                    |        const delta            = firstWeight - lastWeight
                    |        const saOriginX        = wsg.getDate(firstRecord).getTime()
                    |        const sa               = calculateStatistics(records, r => (wsg.getDate(r).getTime() - saOriginX) / 86400000.0, wsg.getWeight)
                    |        const firstWeightTrend = sa.predict(firstRecord)
                    |        const lastWeightTrend  = sa.predict(lastRecord)
                    |    
                    |        return {
                    |            __proto__     : sa,
                    |            delta         : delta,
                    |            percentChange : delta / firstWeight,
                    |            weightRange   : { min: d3.min(records, wsg.getWeight), max: d3.max(records, wsg.getWeight) },
                    |            first         : { index: firstIndex, date: wsg.getDate(firstRecord), weight: firstWeight, trend: firstWeightTrend },
                    |            last          : { index: lastIndex, date: wsg.getDate(lastRecord) , weight: lastWeight,  trend: lastWeightTrend  },
                    |            numRecords    : records.length,        
                    |        }
                    |    }    
                    |}""".trimMargin())
    }.toString()