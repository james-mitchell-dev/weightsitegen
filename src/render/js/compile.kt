/*
 * WeightSiteGen
 * Copyright (C) 2019  James P. Mitchell
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package render.js

import analysis.DateRangeAnalysis
import WeightRecords
import getMergedFileTextFromList
import java.lang.StringBuilder
import com.google.javascript.jscomp.*

//http://blog.bolinfest.com/2009/11/calling-closure-compiler-from-java.html
fun optimizeJS(code: String): String =
    Compiler().apply {
        val options = CompilerOptions().apply { CompilationLevel.SIMPLE_OPTIMIZATIONS.setOptionsForCompilationLevel(this) }
        val externs = CommandLineRunner.getBuiltinExterns(options.environment)
        val inputs = listOf(SourceFile.fromCode("input.js", code))
        compile(externs, inputs, options)
    }.toSource()

fun compileJS(records: WeightRecords, dras: List<DateRangeAnalysis>) =
    optimizeJS(StringBuilder().apply {
        appendln(getMergedFileTextFromList("www/libs.txt"))
        appendln(renderWeightSiteGenJS(records, dras))
        appendln(getMergedFileTextFromList("www/scripts.txt"))
    }.toString())