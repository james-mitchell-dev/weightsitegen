/*
 * WeightSiteGen
 * Copyright (C) 2019  James P. Mitchell
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package render.js

import org.joda.time.DateTime
import java.lang.StringBuilder
import java.text.DecimalFormat

private val FORMAT_JS = DecimalFormat("#.##########")

fun Boolean .toJS():String = if (this) "true" else "false"
fun Double  .toJS():String = FORMAT_JS.format(this)
fun Int     .toJS():String = FORMAT_JS.format(this)
fun Long    .toJS():String = FORMAT_JS.format(this)
fun Float   .toJS():String = FORMAT_JS.format(this)
fun DateTime.toJS():String = "new Date(${millis})"

fun List<Int>.toJS():String = joinToString(
    separator = ",",
    prefix = "[",
    postfix = "]",
    transform = { it.toJS() }
)