/*
 * WeightSiteGen
 * Copyright (C) 2019  James P. Mitchell
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import analysis.Milestones
import analysis.StandardWeights
import analysis.generateDateRangeAnalysis
import analysis.generateYearlyAnalyses
import com.github.ajalt.clikt.core.CliktCommand
import com.github.ajalt.clikt.parameters.arguments.argument
import com.github.ajalt.clikt.parameters.arguments.defaultLazy
import com.github.ajalt.clikt.parameters.options.default
import com.github.ajalt.clikt.parameters.options.flag
import com.github.ajalt.clikt.parameters.options.option
import com.github.ajalt.clikt.parameters.options.versionOption
import com.github.ajalt.clikt.parameters.types.*
import org.joda.time.Period
import render.css.COMPILED_CSS
import render.html.*
import render.js.compileJS

const val VERSION_STRING = "1.2"
val HEIGHT_RANGE = 12.0..120.0
val WRIST_RANGE = 1.0..20.0

class WeightSiteGenerator: CliktCommand(name = "wsg", help = """
    WeightSiteGen is a static webpage generator for weight recordings. It produces a single, self-contained HTML document that can visualize and analyze your weight data over time. 
    
    INPUT_FILE must contain CSV data in the format: "mm/dd/yyyy, 000.0"
    
    All weight values are interpreted as pounds. 
    """.trimIndent()) {
    private val hasColumnNames  by option("-c", "--has-column-names", help = "Indicates that the first CSV row contains column names").flag(default = false)
    private val height          by option("-i", "--height", help="Height in inches. Required for all Milestone calculations. Must be between ${HEIGHT_RANGE.start} and ${HEIGHT_RANGE.endInclusive}").double().restrictTo(HEIGHT_RANGE).default(Double.NaN)
    private val wrist           by option("-w", "--wrist", help = "Wrist circumference in inches. Must be between ${WRIST_RANGE.start} and ${WRIST_RANGE.endInclusive}. Required to calculate Hamwi Ideal Weight Formula").double().restrictTo(0.0..20.0).default(Double.NaN)
    private val gender          by option("-g", "--gender", help="Required to calculate most Ideal Weight Formulae").choice("male", "female")
    private val inputFile       by argument("INPUT_FILE",  help="Path to input CSV file").file(exists = true, folderOkay = false, readable = true)
    private val outputPath      by argument("OUTPUT_PATH", help="Path to output HTML file. If not specified the document will be written to the same directory as INPUT_FILE").path().defaultLazy {
        inputFile.toPath().run {
            resolveSibling(fileName.toString().run {
                    if (endsWith(".csv"))
                        replaceRange(length-4, length, ".html")
                    else
                        "$this.html"
                }
            )
        }
    }

    override fun run() {
        // TODO: Think of the best way to represent one or more null milestones.
        val ct = ConsoleTask()
        val sw = StandardWeights(
            height = height,
            wrist = wrist,
            gender = gender
        )
        val sv = StringVars(
            height = height,
            wrist = wrist,
            gender = gender.orEmpty(),
            version = VERSION_STRING
        )

        try  {
            ct.begin("Parsing CSV file", 25)
            val records = WeightRecords.fromFile(hasColumnNames, inputFile)
            //val records = WeightRecords.fromRandom(DateTime.now().minusYears(5), DateTime.now(), 160.0, 400.0)
            ct.end()

            ct.begin("Analysing Records", 25)

            val m12 = records.takeLastPeriod(Period().plusMonths(12))
            val m06 = m12.takeLastPeriod(Period().plusMonths(6))
            val m02 = m06.takeLastPeriod(Period().plusMonths(2))
            val m01 = m02.takeLastPeriod(Period().plusMonths(1))

            val allAnalysis = sequenceOf(
                generateDateRangeAnalysis("all", "All", records),
                generateDateRangeAnalysis("max", "Max", records.splice(records.maxRange))
            )

            val lastAnalyses = sequenceOf(
                generateDateRangeAnalysis("m01", "1 Mo.", m01),
                generateDateRangeAnalysis("m02", "2 Mo.", m02),
                generateDateRangeAnalysis("m06", "6 Mo.", m06),
                generateDateRangeAnalysis("m12", "1 Yr.", m12)
            )

            val yearlyAnalyses          = generateYearlyAnalyses(records.groupByYear())
            val allAnalyses             = (allAnalysis + lastAnalyses + yearlyAnalyses).toList()
            val milestones              = Milestones(sw, records)
            val latestStrings           = LatestRecordingStrings(sw, records.values.last())
            val milestonesStrings       = MilestonesStrings(sv, milestones)
            val weightRecordStrings     = WeightRecordsStrings(records)
            val allAnalysisStrings      = allAnalysis.map { DateRangeAnalysisStrings(it) }.toList()
            val lastAnalysisStrings     = lastAnalyses.map { DateRangeAnalysisStrings(it) }.toList()
            val yearlyAnalysisStrings   = yearlyAnalyses.map { DateRangeAnalysisStrings(it) }.toList()
            ct.end()

            ct.begin("Compiling Javascript", 25)
            val compiledJS = compileJS(records, allAnalyses)
            ct.end()

            ct.begin("Rendering HTML", 25)

            val foreword = getForeword(sv)
            val epilogue = getEpilogue(sv)

            val htmlString = renderHTML(
                COMPILED_CSS,
                compiledJS,
                foreword,
                epilogue,
                latestStrings,
                milestonesStrings,
                weightRecordStrings,
                allAnalysisStrings,
                lastAnalysisStrings,
                yearlyAnalysisStrings
            )
            ct.end()

            ct.begin("Writing HTML file", 25)


            outputPath.toFile().writeText(htmlString)
            ct.end()

        } catch (e: Exception) {
            ct.end(e.message ?: "Fail")
        }
    }
}

fun main(args: Array<String>) = WeightSiteGenerator().versionOption(VERSION_STRING).main(args)