/*
 * WeightSiteGen
 * Copyright (C) 2019  James P. Mitchell
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import java.lang.Exception

private val CONTEXT_CLASS_LOADER = Thread.currentThread().contextClassLoader

fun getResourceFileStream(path: String)
    = CONTEXT_CLASS_LOADER.getResourceAsStream(path)
        ?: throw Exception("Failed to open stream to resource: '${path}'")

fun getResourceFileText(path: String) =
    getResourceFileStream(path).bufferedReader().use { it.readText() }

fun getResourceFileTextFromList(path: String) =
    getResourceFileText(path)
        .lineSequence()
        .filter { it.isNotEmpty() }
        .map { Pair(it, getResourceFileText(it)) }

fun getMergedFileTextFromList(path: String) =
    getResourceFileTextFromList(path)
        .map { (_, text) -> text }
        .joinToString("\n\n")