/*
 * WeightSiteGen
 * Copyright (C) 2019  James P. Mitchell
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import org.apache.commons.csv.CSVFormat
import org.apache.commons.csv.CSVPrinter
import org.apache.commons.csv.CSVRecord
import org.joda.time.DateTime
import org.joda.time.Duration
import org.joda.time.Period
import render.toCSV
import render.toDateTime
import java.io.File
import kotlin.random.Random


typealias Weight = Double

data class WeightRecord(val date: DateTime, val weight: Double, val index: Int, val comment: String)

data class YearlyWeightRecords(val year: Int, val records: WeightRecords)

class WeightRecords private constructor(val values: List<WeightRecord>) {

    //fun interpolate() = WeightRecords(interpolate(values))

    fun takeLastPeriod(period: Period) : WeightRecords {
        val timestamp = values.last().date.minus(period)
        val index = values.indexOfFirst { it.date.isAfter(timestamp) }
        return splice(index)
    }

    fun splice(startIndex: Int, endIndex: Int) = WeightRecords(values.subList(startIndex, endIndex))
    fun splice(startIndex: Int) = WeightRecords(values.subList(startIndex, values.size))
    fun splice(range: IntRange) = WeightRecords(values.subList(range.first, range.last+1))

    val maxRange get() : IntRange {
        val min = values.minBy { it.weight }!!
        val max = values.maxBy { it.weight }!!
        if (min.index < max.index)
            return min.index..max.index
        return max.index..min.index
    }

    fun next(wr: WeightRecord): WeightRecord? {
        val index = wr.index+1
        return if (index in values.indices) values[index]
        else null
    }

    fun prev(wr: WeightRecord): WeightRecord? {
        val index = wr.index-1
        return if (index in values.indices) values[index]
        else null
    }

    fun groupByYear() = values
        .asSequence()
        .groupBy { r -> r.date.year }
        .map { e ->
            YearlyWeightRecords(
                e.key,
                WeightRecords(e.value)
            )
        }

    fun toFile(file: File) {
        writeWeightRecords(this, file)
    }

    companion object {
        fun fromFile(hasColumnNames: Boolean, file: File): WeightRecords =
            WeightRecords(readWeightRecords(hasColumnNames, file)).apply {
                if (values.size < 2) {
                    throw Exception("Data set must contain at least 2 weight recordings")
                }
            }

        fun fromRandom(begin: DateTime, end: DateTime, minWeight: Double, maxWeight: Double) =
            WeightRecords(createRandomWeightRecords(begin, end, minWeight, maxWeight))
    }
}

private class CsvRecordParser {
    val repeatMap = HashMap<Long, Int>()

    fun parseCsvRecord(lineNumber: Long, row: CSVRecord) : Triple<DateTime, Weight, CharSequence> {
        val sz = row.size()

        if (sz < 2)
            throw Exception("Failed to parse row on line $lineNumber: \"${row}\"")

        var date = try {
            row[0].toDateTime()
        } catch (e: Exception) {
            throw Exception("Failed to parse date on line $lineNumber: \"${row[0]}\"", e)
        }

        val repeat = repeatMap.get(date.millis)
        if (repeat != null) {
            val repeatNext =  repeat + 1
            if (repeatNext >= 1440) {
                throw Exception("Too many records for date ${date.toCSV()}")
            }
            repeatMap.set(date.millis, repeatNext)
            date = date.plusMinutes(repeatNext)
        } else {
            repeatMap.set(date.millis, 0)
        }

        val weight = try {
            row[1].toDouble()
        } catch (e: Exception) {
            throw Exception ("Failed to parse weight on line $lineNumber: \"${row[1]}\"", e)
        }

        if (weight !in 10.00..10000.0)
            throw Exception ("Weight is invalid on line $lineNumber: \"${row[1]}\"")

        return Triple(date, weight, if (sz > 2) row[2] else "")
    }
}

private fun readWeightRecords(hasColumnNames: Boolean, file: File) =
    file.inputStream().use {stream ->
        stream.reader().use { streamReader ->
            CSVFormat.DEFAULT.withSkipHeaderRecord(!hasColumnNames).run {
                parse(streamReader).use { records ->
                    CsvRecordParser().run {
                        records
                            .map { parseCsvRecord(records.currentLineNumber, it) }
                            .sortedBy { record -> record.first.millis }
                            .mapIndexed{ rowIndex, row -> WeightRecord(row.first, row.second, rowIndex, row.third.toString()) }
                            .toList()
                    }
                }
            }
        }
    }

private fun writeWeightRecords(weightRecords: WeightRecords, file: File) {
    file.outputStream().use { stream ->
        stream.writer().use { streamWriter ->
            CSVPrinter(streamWriter, CSVFormat.DEFAULT.withSkipHeaderRecord(true)).use { printer ->
                weightRecords.values.forEach {
                    printer.printRecord(it.date.toCSV(), it.weight.toCSV())
                }
            }
        }
    }
}

private fun createRandomWeightPath(minWeight: Double, maxWeight: Double): List<Double> {
    val weights = mutableListOf<Double>()

    var weight = maxWeight
    while (weight > minWeight) {
        val fuzz = 5
        val skew = 0.45

        val max = fuzz * skew
        val min = max - fuzz

        weight += Random.nextDouble(min, max)
        weights.add(weight)
    }

    return weights
}

private fun createRandomWeightRecords(begin: DateTime, end: DateTime, minWeight: Double, maxWeight: Double) : List<WeightRecord> {
    assert(begin.isBefore(end))
    assert(minWeight < maxWeight)

    val weeks = Duration(begin, end).standardDays / 7

    assert(weeks > 0)
    assert(weeks < Int.MAX_VALUE)

    val records = mutableListOf<WeightRecord>()
    val weightPath = createRandomWeightPath(minWeight, maxWeight)

    for (week in 0 until weeks.toInt()) {
        val x = week.toDouble() / weeks.toDouble()
        val weight = interpolate(weightPath, x)
        val date = begin.plusWeeks(week)

        records.add(WeightRecord(date,weight,week, ""))
    }

    return records
}

private fun interpolate(x: Double, y: Double, t: Double): Double {
    return x*(1-t)+y*t
}

fun interpolate(path: List<Double>, t: Double):Double {
    assert(t >= 0.0)
    assert(t <= 1.0)

    val ti = t * path.size
    val i  = ti.toInt()
    val tt = (ti - i.toDouble())

    return interpolate(path[i], path[i+1], tt)
}

//private fun interpolate(records: List<WeightRecord>) = records.asSequence()
//    .zipWithNext()
//    .flatMap { p ->
//        val diff = Duration(p.first.date, p.second.date)
//
//        if (diff.standardDays > 1)
//            (0 until diff.standardDays).asSequence().map {
//                val t = 1.0 / diff.standardDays.toDouble() * it
//                val date = p.first.date.plusDays(it.toInt())
//                val weight = lerp(p.first.weight, p.second.weight, t)
//                WeightRecord(date, weight, 0, p.first.comment)
//            }
//        else
//            sequenceOf(p.first)
//    }
//    .plus(records.last())
//    .mapIndexed { index, wr -> WeightRecord(wr.date, wr.weight, index, wr.comment) }
//    .toList()
//

