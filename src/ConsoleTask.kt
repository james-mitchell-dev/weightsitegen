/*
 * WeightSiteGen
 * Copyright (C) 2019  James P. Mitchell
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import kotlin.concurrent.thread

/*
 * WeightSiteGen
 * Copyright (C) 2019  James P. Mitchell
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

class ConsoleTask {
    private var thread: Thread? = null
    private var stop: Boolean = false

    fun begin(task: String, pad:Int = 0) {
        if (thread == null) {
            print("${task.padEnd(pad)}: ")
            thread = spinner()
        }
    }

    fun end(msg:String = "done") {
        if (thread != null) {
            stop = true
            thread!!.join()
            stop = false
            thread = null
            println(msg)
        }
    }

    private fun spinner() =
        thread {
            val a = "|/-\\"
            while (!stop) {
                for (i in 0..3) {
                    print(a[i])
                    Thread.sleep(250)
                    print('\b')
                }
            }
        }
}