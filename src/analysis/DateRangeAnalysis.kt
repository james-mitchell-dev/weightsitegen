/*
 * WeightSiteGen
 * Copyright (C) 2019  James P. Mitchell
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package analysis

import Weight
import WeightRecord
import WeightRecords
import YearlyWeightRecords

private const val MILLIS_DAY = 86400000.0

data class WeightRecordTrend(val wr: WeightRecord, val trend: Weight) {
    inline val date   get() = wr.date
    inline val weight get() = wr.weight
    inline val index  get() = wr.index
}

data class WeightRange(val min: Weight, val max: Weight)

data class WeekDayIndices(
    val monday: List<Int>,
    val tuesday: List<Int>,
    val wednesday: List<Int>,
    val thursday: List<Int>,
    val friday: List<Int>,
    val saturday: List<Int>,
    val sunday: List<Int>
)

data class DateRangeAnalysis (
    val key         : String,
    val name        : String,
    val numRecords  : Int,
    val first       : WeightRecordTrend,
    val last        : WeightRecordTrend,
    val weightRange : WeightRange,
    val sa          : StatisticalAnalysis,
    val wdi         : WeekDayIndices
) {
    inline val mean      get() = sa.mean
    inline val ser       get() = sa.ser
    inline val mad       get() = sa.mad
    inline val slope     get() = sa.slope
    inline val intercept get() = sa.intercept

    val delta = first.weight - last.weight
    val percentChanged = delta / first.weight

    //private val lrOriginX = first.date.millis
    //private fun predict(dt: DateTime) = sa.predict((dt.millis - lrOriginX).toDouble() / MILLIS_DAY)
}

fun generateDateRangeAnalysis(key: String, name: String, records: WeightRecords): DateRangeAnalysis {
    val firstRecord = records.values.first()
    val lastRecord  = records.values.last()
    val saOriginX   = firstRecord.date.millis
    val saGetX      = { it: WeightRecord -> (it.date.millis - saOriginX).toDouble() / MILLIS_DAY }
    val saGetY      = { it: WeightRecord -> it.weight }
    val sa          = records.values.statisticalAnalysisBy(saGetX, saGetY)
    val weightRange = WeightRange(
        records.values.minBy { it.weight }!!.weight,
        records.values.maxBy { it.weight }!!.weight
    )
    val wdi = records.values.asSequence()
        .groupBy { it.date.dayOfWeek }
        .mapValues { e -> e.value.map { v -> v.index } }
        .run {
            WeekDayIndices(
                get(1) ?: emptyList(),
                get(2) ?: emptyList(),
                get(3) ?: emptyList(),
                get(4) ?: emptyList(),
                get(5) ?: emptyList(),
                get(6) ?: emptyList(),
                get(7) ?: emptyList()
            )
        }

    return DateRangeAnalysis(
        key,
        name,
        records.values.size,
        WeightRecordTrend(firstRecord, sa.predict(saGetX(firstRecord))),
        WeightRecordTrend(lastRecord, sa.predict(saGetX(lastRecord))),
        weightRange,
        sa,
        wdi
    )
}

fun generateYearlyAnalyses(records: List<YearlyWeightRecords>) = records.asSequence()
    .sortedByDescending{ wr -> wr.year }
    .map { wr ->
        generateDateRangeAnalysis(
            "y${wr.year}",
            wr.year.toString(),
            wr.records
        )
    }



