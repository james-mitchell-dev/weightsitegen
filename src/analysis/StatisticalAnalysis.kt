/*
 * WeightSiteGen
 * Copyright (C) 2019  James P. Mitchell
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package analysis

import kotlin.math.abs

// https://algs4.cs.princeton.edu/code/edu/princeton/cs/algs4/LinearRegression.java
// https://www.codeproject.com/Articles/576228/Line-Fitting-in-Images-Using-Orthogonal-Linear-Reg

data class StatisticalAnalysis(
    val mean: Double,
    val intercept: Double,
    val slope: Double,
    val ser: Double,
    val mad: Double
) {
    fun predict(x: Double) = slope * x + intercept
}

fun pow2(x: Double) = x * x

inline fun <T> Collection<T>.averageByDouble(selector: (T) -> Double) = sumByDouble(selector) / size

inline fun <T> Collection<T>.statisticalAnalysisBy(x: (T) -> Double, y: (T) -> Double) : StatisticalAnalysis {
    if (size <= 0) {
        return StatisticalAnalysis(
            Double.NaN,
            Double.NaN,
            Double.NaN,
            Double.NaN,
            Double.NaN
        )
    }

    if (size == 1) {
        val v = y(first())
        return StatisticalAnalysis(v, v, 0.0, 0.0, 0.0)
    }

    val xMean = averageByDouble(x)
    val yMean = averageByDouble(y)

    val xxBar = sumByDouble { pow2(x(it) - xMean) }
    val xyBar = sumByDouble { (x(it) - xMean) * (y(it) - yMean) }

    val slope = xyBar / xxBar
    val intercept = yMean - slope * xMean

    val ser = averageByDouble { abs(slope * x(it) + intercept - y(it)) }
    val mad = averageByDouble { abs(y(it) - yMean) }

    return StatisticalAnalysis(yMean, intercept, slope, ser, mad)
}