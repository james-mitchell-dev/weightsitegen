/*
 * WeightSiteGen
 * Copyright (C) 2019  James P. Mitchell
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package analysis

import Weight
import java.text.DecimalFormat

const val BMI_CONSTANT          = 703.00
const val FACTOR_KILO_TO_POUND  = 2.20462
const val FACTOR_INCHES_TO_CM   = 2.54
const val BMI_OBESE_CLASS3      = 40.00
const val BMI_OBESE_CLASS2      = 35.00
const val BMI_OBESE_CLASS1      = 30.00
const val BMI_OVERWEIGHT        = 25.00
const val BMI_UNDERWEIGHT       = 18.50

class StandardWeights(
    height: Double = Double.NaN,
    wrist: Double = Double.NaN,
    gender: String? = null
) {
    private val heightI        = height
    private val heightCM       = heightI * FACTOR_INCHES_TO_CM
    private val heightMSquared = heightCM * heightCM / 10000
    private val heightIOver60  = heightI - 60.00
    private val heightISquared = heightI * heightI
    private val heightCMOver100 = heightCM - 100.00
    private val isMale =
        when (gender) {
            "male" -> true
            "female" -> false
            else -> null
        }

    private val wristFactor =
        when {
            wrist.isNaN() -> Double.NaN
            wrist < 7.0 -> 0.90
            wrist > 7.0 -> 1.10
            else -> 1.0
        }

    private val factorBmiToWeight   = heightISquared / BMI_CONSTANT
    private val factorWeightToBmi   = BMI_CONSTANT / heightISquared

    val minObeseClass3   = (BMI_OBESE_CLASS3) * factorBmiToWeight
    val minObeseClass2   = (BMI_OBESE_CLASS2) * factorBmiToWeight
    val minObeseClass1   = (BMI_OBESE_CLASS1) * factorBmiToWeight
    val minOverweight    = (BMI_OVERWEIGHT) * factorBmiToWeight
    val minHealthyWeight = (BMI_UNDERWEIGHT) * factorBmiToWeight
    val formulaHamwi     = (heightIOver60 * g(2.70, 2.20) + g(48.0, 45.50)) * FACTOR_KILO_TO_POUND * wristFactor
    val formulaDevine    = (heightIOver60 * g(2.30, 2.30) + g(50.0, 45.50)) * FACTOR_KILO_TO_POUND
    val formulaRobinson  = (heightIOver60 * g(1.90, 1.70) + g(52.0, 49.00)) * FACTOR_KILO_TO_POUND
    val formulaMiller    = (heightIOver60 * g(1.41, 1.36) + g(56.2, 53.10)) * FACTOR_KILO_TO_POUND
    val formulaLemmens   = (heightMSquared * 22.00 ) * FACTOR_KILO_TO_POUND
    val formulaBroca     = (heightCMOver100 * g(0.90, 0.85)) * FACTOR_KILO_TO_POUND

    fun bmi(weight: Weight) = weight * factorWeightToBmi

    private fun g(m: Double, f:Double) =
        when (isMale) {
            true  -> m
            false -> f
            else  -> Double.NaN
        }

    val genderString =
        when (isMale) {
            true  -> "male"
            false -> "female"
            else  -> "n/a"
        }

    val heightString: String get() {
        val inchFmt = DecimalFormat("0.#")
        val feet = heightI.toInt() / 12
        val inches = heightI % 12.0
        return "$feet′ ${inchFmt.format(inches)}″ "
    }
}
