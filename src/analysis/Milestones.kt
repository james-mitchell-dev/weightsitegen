/*
 * WeightSiteGen
 * Copyright (C) 2019  James P. Mitchell
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package analysis

import Weight
import WeightRecords
import org.joda.time.DateTime
import org.joda.time.Period
import org.joda.time.PeriodType

enum class MilestoneType {
    Underweight,
    Miller,
    Robinson,
    Devine,
    Hamwi,
    //Normalweight,
    Lemmens,
    Broca,
    Overweight,
    ObesityClass1,
    ObesityClass2,
    ObesityClass3
}

enum class MilestoneStatus {
    Lost,
    Gained,
    NeverBelow,
    NeverAbove
}

data class Milestone(val type: MilestoneType, val status: MilestoneStatus, val isWanted: Boolean, val weight: Weight, val delta: Weight, val date: DateTime?, val period: Period?)

class Milestones(sw: StandardWeights, private val records: WeightRecords) {
    val obese3      = createMilestone(MilestoneType.ObesityClass3, sw.minObeseClass3)
    val obese2      = createMilestone(MilestoneType.ObesityClass2, sw.minObeseClass2)
    val obese1      = createMilestone(MilestoneType.ObesityClass1, sw.minObeseClass1)
    val overweight  = createMilestone(MilestoneType.Overweight,    sw.minOverweight)
    val lemmens     = createMilestone(MilestoneType.Lemmens,       sw.formulaLemmens)
    val broca       = createMilestone(MilestoneType.Broca,         sw.formulaBroca)
    val hamwi       = createMilestone(MilestoneType.Hamwi,         sw.formulaHamwi)
    val devine      = createMilestone(MilestoneType.Devine,        sw.formulaDevine)
    val robinson    = createMilestone(MilestoneType.Robinson,      sw.formulaRobinson)
    val miller      = createMilestone(MilestoneType.Miller,        sw.formulaMiller)
    val underweight = createMilestone(MilestoneType.Underweight,   sw.minHealthyWeight, false)

    private fun createMilestone(type: MilestoneType, weight: Weight, isWanted: Boolean = true) : Milestone? {
        if (weight.isNaN())
            return null

        val current = records.values.last()

        val delta = weight - current.weight

        val date = records.values.findLast {
            val prev = records.prev(it)
            prev != null && prev.weight > weight && it.weight < weight
        }?.date

        val status = if (date == null) {
            if (delta > 0) {
                MilestoneStatus.NeverAbove
            } else {
                MilestoneStatus.NeverBelow
            }
        } else {
            if (delta > 0) {
                MilestoneStatus.Gained
            } else {
                MilestoneStatus.Lost
            }
        }

        val period = date?.let { Period(date, current.date, PeriodType.yearMonthDay()) }

        return Milestone(type, status, isWanted, weight, delta, date, period)
    }
}