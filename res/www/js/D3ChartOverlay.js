class D3ChartOverlay {
    constructor(chartWidth, chartHeight, chartSvg, isEnabled) {

        this.chartWidth = chartWidth
        this.chartHeight = chartHeight

        this.svgElements = []
        this.tooltipRight = this.createTooltip(chartSvg, 0, "1em")
        this.tooltipLeft = this.createTooltip(chartSvg, this.chartHeight, "-1.2em")
        this.isEnabled = isEnabled
    }

    createTooltip(svg, height, hOffset) {
        const vOffset = 10 

        const svgTooltip = svg.append('g').append('text')
            .attr("class", "tooltip")    
            .attr("text-anchor", "start")
            .style("opacity", 0)
            .attr("pointer-events", "none")

        const svgTextLine1 = svgTooltip.append('tspan').attr('dy', hOffset)
        const svgTextLine2 = svgTooltip.append('tspan').attr('dy', "1em")

        const svgFocus = svg.append('g').append('circle')
            .attr("class", "tooltip-focus")
            .attr("pointer-events", "none")
            .attr('r', 4)
            .style("opacity", 0)

        const svgLine = svg.append('g').append('line')
            .attr("class", "tooltip-line")
            .attr("stroke-dasharray", "5,5")
            .style("opacity", 0)
            .attr("pointer-events", "none")

        var indexX = undefined

        this.svgElements.push(svgTooltip, svgFocus, svgLine)

        function setTextXY(x, y) {
            svgTooltip.attr("y", y)
            svgTooltip.attr("x", x)
            svgTextLine1.attr("x",x + vOffset)
            svgTextLine2.attr("x",x + vOffset)
        }

        function setTextLines(l1, l2) {
            svgTextLine1.html(l1)
            svgTextLine2.html(l2)
        }

        var tooltip = {
            getIndexX: () => {
                return indexX
            },

            show: () => {
                svgFocus.style('opacity', 1)
                if (this.isEnabled) {
                    svgTooltip.style('opacity', 1)
                    svgLine.style('opacity', 1)
                }
            },
        
            hide: () => {
                svgFocus.style('opacity', 0)
                if (this.isEnabled) {
                    svgTooltip.style('opacity', 0)
                    svgLine.style('opacity', 0)
                }
            },

            update: (xIndex, linePositionX, focusPosition, focusData) => {        
                indexX = xIndex
                
                svgFocus
                    .attr("cx", focusPosition[0])
                    .attr("cy", focusPosition[1])
        
                svgLine
                    .attr("x1", linePositionX)
                    .attr("y1", 0)
                    .attr("x2", linePositionX)
                    .attr("y2", this.chartHeight)
        
                setTextLines(
                    focusData[0].toDateString(),
                    (Math.round(focusData[1] * 10) / 10).toString()
                )
                    
                const bbox = svgTooltip.node().getBBox()
                const maxX = this.chartWidth - bbox.width - vOffset
                
                setTextXY(
                    Math.min(maxX, linePositionX),
                    height
                )
            }
        }

        return tooltip
    }

    setEnabled(isEnabled) {
        this.isEnabled = isEnabled
    }

    update(cur, pos, d) {
        this.tooltipLeft.update(cur[0], pos, d)
        this.tooltipRight.update(cur[0] + 100, pos, d)
    }

    showAll() {
        this.svgElements.forEach(e => e.style('opacity', 1))
    }

    hideAll() {
        this.svgElements.forEach(e => e.style('opacity', 0))
    }
}