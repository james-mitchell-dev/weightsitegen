"use strict";

window.onload = function() {
    const dom = {
        divChart      : document.getElementById("chart"),
        cboxTrendLine : document.getElementById("cboxTrendLine"),
        cboxMeanLine  : document.getElementById("cboxMeanLine"),
        cboxTooltip   : document.getElementById("cboxTooltip"),
    };

    const analysisAll = wsg.analyses["all"]
    const chartAnalysisView = new ChartAnalysisView(analysisAll)
    var d3Chart = new D3Chart(
        dom.divChart, 
        analysisAll, 
        dom.cboxTrendLine.checked, 
        dom.cboxMeanLine.checked, 
        dom.cboxTooltip.checked,
        analysis => chartAnalysisView.setAnalysis(analysis)
    )

    var resizeTimeout
    var previousChartSize = [dom.divChart.clientWidth, dom.divChart.clientHeight]
    
    window.onresize = function() {
        clearTimeout(resizeTimeout);
        resizeTimeout = setTimeout(function() {
            const chartSize = [dom.divChart.clientWidth, dom.divChart.clientHeight]
            if (previousChartSize[0] !== chartSize[0] || previousChartSize[1] !== chartSize[1]) {
                d3Chart.delete()
                d3Chart = d3Chart.clone()
                previousChartSize = chartSize
            }
        }, 250)
        d3Chart.resize()
    }

    $('[data-toggle="popover"]').popover()

    document.querySelectorAll("button#btnDateRange").forEach(e => {
        const key = e.dataset["analysis"]
        e.onclick = () => {
            const analysis = wsg.analyses[key]
            d3Chart.setAnalysis(analysis, true)
            chartAnalysisView.setAnalysis(analysis)
        }
    })

    dom.cboxMeanLine.onclick = () => d3Chart.setMeanLineEnabled(dom.cboxMeanLine.checked, true)
    dom.cboxTrendLine.onclick = () => d3Chart.setTrendLineEnabled(dom.cboxTrendLine.checked, true)
    dom.cboxTooltip.onclick = () => d3Chart.setTooltipEnabled(dom.cboxTooltip.checked, true)
}