"use strict";

class ChartAnalysisView {
    constructor(analysis) {
        this.selFirstDate       = document.getElementById("selFirstDate")
        this.selLastDate        = document.getElementById("selLastDate")
        this.selDateRange       = document.getElementById("selDateRange")
        this.selFirstRecord     = document.getElementById("selFirstRecord")
        this.selLastRecord      = document.getElementById("selLastRecord")
        this.selRecordRange     = document.getElementById("selRecordRange")
        this.selDelta           = document.getElementById("selDelta")
        this.selPercentChange   = document.getElementById("selPercentChange")
        this.selMean            = document.getElementById("selMean") 
        this.selMad             = document.getElementById("selMad")
        this.selSER             = document.getElementById("selSER")
        
        this.setAnalysis(analysis)
    }

    setAnalysis(analysis) {
        this.analysis = analysis
        this.selFirstDate.innerHTML     = formatDate            (analysis.first.date)
        this.selLastDate.innerHTML      = formatDate            (analysis.last.date)
        this.selDateRange.innerHTML     = formatDateRange       (analysis.first.date, analysis.last.date)
        this.selFirstRecord.innerHTML   = formatWeight          (analysis.first.weight)
        this.selLastRecord.innerHTML    = formatWeight          (analysis.last.weight)
        this.selRecordRange.innerHTML   = formatWeightRange     (analysis.numRecords)
        this.selDelta.innerHTML         = formatWeight          (analysis.delta)
        this.selPercentChange.innerHTML = formatPercentChange   (analysis.percentChange)
        this.selMean.innerHTML          = formatWeight          (analysis.mean) 
        this.selMad.innerHTML           = formatStat            (analysis.mad)
        this.selSER.innerHTML           = formatStat            (analysis.ser)
    }
}