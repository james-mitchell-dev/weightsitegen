"use strict";

class D3Chart {
    constructor(chart, analysis, isTrendLineEnabled, isMeanLineEnabled, isTooltipEnabled, onWeightRangeChanged) {
        this.ticksWeight     = 5
        this.ticksDate       = 5
        this.fontSize        = 12.8
        this.animDuration    = 1000
        this.margin          = {top: this.fontSize, right: this.fontSize, bottom: this.fontSize * 2, left: this.fontSize * 3.5}
        this.isBrushing      = false

        this.initializeSvg(chart)
        
        this.xScale   = d3.scaleTime().range([0, this.chartAreaWidth])   
        this.yScale   = d3.scaleLinear().range([this.chartAreaHeight, 0])
        this.line     = d3.line().x(d => this.xScale(d[0])).y(d => this.yScale(d[1]))
        this.bisector = d3.bisector(wsg.getDate)
        this.brush    = d3.brushX()
            .extent([[0, 0], [this.chartAreaWidth, this.chartAreaHeight]])  // initialise the brush area: start at 0,0 and finishes at width,height: it means I select the whole graph area
            
            .on("start", () => {
                const extent = d3.event.selection
                if (extent) {
                    this.isBrushing = true
                    this.overlay.tooltipLeft.show()
                    this.overlay.tooltipRight.show()
                    this.updateBothTooltips(extent[0], extent[1])
                }
            })
            .on("end", () => {
                if (d3.event.selection) {
                    this.isBrushing = false
                    this.setAnalysisFromTooltips()
                    this.clearBrush()
                    this.overlay.hideAll()

                    setTimeout(() => this.overlay.tooltipLeft.show(), this.animDuration + 100)
                }
            })
            .on("brush", () => {
                const extent = d3.event.selection
                if (extent) {
                    this.updateBothTooltips(extent[0], extent[1])
                }
            })

        this.svgLine
            .on("mouseover", () => {
                if (!this.isBrushing) {
                    this.overlay.tooltipLeft.show()
                }
            })
            .on('mouseout', () => {
                if (!this.isBrushing) {
                    this.overlay.tooltipLeft.hide()
                }
            })

            .on('mousemove', () => {
                if (!this.isBrushing) {
                    const pCursor = d3.mouse(d3.event.currentTarget)
                    this.overlay.tooltipLeft.update.apply(
                        this.overlay.tooltipLeft,
                        this.getTooltipUpdate(Math.max(0, Math.min(this.chartAreaWidth, pCursor[0])))
                    )
                }
            })
            
            
        this.svgLinePath
            .datum(wsg.data)

        this.svgLineBrush.call(this.brush)

        this.setAnalysis(analysis)
        this.setMeanLineEnabled(isMeanLineEnabled)
        this.setTrendLineEnabled(isTrendLineEnabled)

        this.overlay = new D3ChartOverlay(this.chartAreaWidth, this.chartAreaHeight, this.svg, isTooltipEnabled)
        this.onWeightRangeChanged = onWeightRangeChanged
    }

    initializeSvg(chart) {
        this.chart           = chart
        this.svgWidth        = this.chart.clientWidth
        this.svgHeight       = this.chart.clientHeight
        this.chartAreaWidth  = this.svgWidth - this.margin.left - this.margin.right
        this.chartAreaHeight = this.svgHeight - this.margin.top - this.margin.bottom

        this.svgParent = d3.select(chart)
        this.svgRoot = this.svgParent 
            .append("svg")
            .attr("viewBox", `0 0 ${this.svgWidth} ${this.svgHeight}`)
            .style("position", "absolute")
            .attr("preserveAspectRatio", "none")
            .attr("width", this.svgWidth)
            .attr("height", this.svgHeight)

        this.svg = this.svgRoot.append('g')
            .attr("transform", `translate(${this.margin.left},${this.margin.top})`)

        this.svgXAxis = this.svg.append("g")
            .attr("class", "axis")    
            .attr("transform", `translate(0,${this.chartAreaHeight})`)

        this.svgYAxis = this.svg.append("g")
            .attr("class", "axis")

        const svgClip = this.svg.append("defs").append("svg:clipPath")
            .attr("id", "clip") 
            .append("svg:rect")
            .attr("width", this.chartAreaWidth)
            .attr("height", this.chartAreaHeight)
            .attr("x", 0)
            .attr("y", 0)
     
        this.svgLine = this.svg.append('g')
            .attr("clip-path", "url(#clip)")

        this.svgTrendLine = this.svg.append('g').append('line')
            .attr("class", "trend-line")    
            .attr("pointer-events", "none")
            .attr("stroke-width", 1.5)
            .attr("clip-path", "url(#clip)")

        this.svgMeanLine = this.svg.append('g').append('line')
            .attr("class", "mean-line")
            .attr("pointer-events", "none")
            .attr("stroke-width", 1.5)
            .attr("clip-path", "url(#clip)")

        this.svgLinePath = this.svgLine.append("path")
            .attr("class", "line")
            .attr("fill", "none")
            .attr("stroke", "white")
            .attr("stroke-width", 1)
            .attr("pointer-events", "none")

        this.svgLineBrush = this.svgLine.append('g')
            .attr("class", "brush")
    }

    clone() {
        return new D3Chart(this.chart, this.analysis, this.isTrendLineEnabled, this.isMeanLineEnabled, this.overlay.isEnabled, this.onWeightRangeChanged)
    }

    setTrendLineEnabled(isEnabled, isTransition) {
        if (this.isTrendLineEnabled !== isEnabled) {
            this.isTrendLineEnabled = isEnabled
            if (isTransition)
                this.svgTrendLine.transition().duration(150).style("opacity", isEnabled ? 1 : 0)
            else
                this.svgTrendLine.style("opacity", isEnabled ? 1 : 0)
        }
    }

    setMeanLineEnabled(isEnabled, isTransition) {
        if (this.isMeanLineEnabled !== isEnabled) {
            this.isMeanLineEnabled = isEnabled
            if (isTransition)
                this.svgMeanLine.transition().duration(150).style("opacity", isEnabled ? 1 : 0)
            else 
                this.svgMeanLine.style("opacity", isEnabled ? 1 : 0)
        }
    }

    setTooltipEnabled(isEnabled) {
        this.overlay.setEnabled(isEnabled)
    }

    resize() {
        this.svgRoot
            .attr("width", this.chart.clientWidth)
            .attr("height", this.chart.clientHeight)
    }

    delete() {
        this.svgParent.selectAll("*").remove()
    }
 
    clearBrush() {
        this.svgLine.select(".brush").call(this.brush.move, null)
    } 

    setAnalysis(analysis, isTransition) {
        this.analysis = analysis
        this.changeWeightRange(analysis, isTransition)
        this.changeTrendLines(analysis, isTransition)
    }

    setAnalysisFromTooltips() {
        const xIndexLeft = this.overlay.tooltipLeft.getIndexX()
        const xIndexRight = this.overlay.tooltipRight.getIndexX()

        if (xIndexLeft !== this.analysis.first.index || xIndexRight !== this.analysis.last.index) {
            const records = wsg.data.slice(xIndexLeft, xIndexRight+1) //this.getRecordsInDomain(/*[this.xScale.invert(extent[0]), this.xScale.invert(extent[1])]*/)
        
            if (records.length > 1) {
                
                const analysis = wsg.createAnalysis(records, xIndexLeft, xIndexRight)
                if (this.onWeightRangeChanged) {
                    this.onWeightRangeChanged(analysis)
                }

                this.setAnalysis(analysis, true)
            }
        }
    }

    changeWeightRange(analysis, isTransition) {
        this.xScale.domain([analysis.first.date, analysis.last.date])
        this.yScale.domain([analysis.weightRange.min - 15, analysis.weightRange.max + 15])

        if (isTransition) {
            this.svgXAxis.transition().duration(this.animDuration).call(d3.axisBottom(this.xScale).ticks(this.ticksDate)) 
            this.svgYAxis.transition().duration(this.animDuration).call(d3.axisLeft(this.yScale).ticks(this.ticksWeight))
            this.svgLinePath.transition().duration(this.animDuration).attr("d", this.line)
        }
        else {
            this.svgXAxis.call(d3.axisBottom(this.xScale).ticks(this.ticksDate)) 
            this.svgYAxis.call(d3.axisLeft(this.yScale).ticks(this.ticksWeight))
            this.svgLinePath.attr("d", this.line)
        }
    }

    changeTrendLines(analysis, isTransition) {
        const x1    = this.xScale(analysis.first.date)
        const y1    = this.yScale(analysis.first.trend)
        const x2    = this.xScale(analysis.last.date)
        const y2    = this.yScale(analysis.last.trend)
        const yMean = this.yScale(analysis.mean)

        const slope = (y2 - y1) / (x2 - x1)
        const intercept = y1 - slope*x1 

        const x0 = 0
        const y0 = intercept

        const x3 = this.svgWidth
        const y3 = slope * x3 + intercept

        if (isTransition) {
            this.svgTrendLine.transition().duration(this.animDuration)
                .attr("x1", x0).attr("y1", y0).attr("x2", x3).attr("y2", y3)
            this.svgMeanLine.transition().duration(this.animDuration)
                .attr("x1", x0).attr("y1", yMean).attr("x2", x3).attr("y2", yMean)
        }
        else {
            this.svgTrendLine.attr("x1", x0).attr("y1", y0).attr("x2", x3).attr("y2", y3)
            this.svgMeanLine.attr("x1", x0).attr("y1", yMean).attr("x2", x3).attr("y2", yMean)
        }
    }

    getTooltipUpdate(linePositionX) {
        const xInterpolated     = this.xScale.invert(linePositionX);
        const xIndexRight       = this.bisector.left(wsg.data, xInterpolated,1)

        //if (xIndexRight < 0 || xIndexRight >= wsg.data.length)
        //    return 

        const xIndexLeft        = xIndexRight-1
        const dRight            = wsg.data[xIndexRight]
        const dLeft             = wsg.data[xIndexLeft]
        const xRight            = this.xScale(dRight[0])
        const xLeft             = this.xScale(dLeft[0])

        if (xRight - linePositionX <= linePositionX - xLeft) 
            return [xIndexRight, linePositionX, [xRight, this.yScale(dRight[1])], dRight]
        return [xIndexLeft, linePositionX, [xLeft, this.yScale(dLeft[1])], dLeft]
    }

    // hideTooltip() {
    //     this.overlay.tooltipLeft.hide()
    // }

    updateLeftTooltip(left) {
        this.overlay.tooltipLeft.update.apply(
            this.overlay.tooltipLeft,
            this.getTooltipUpdate(left)
        )
    }

    updateBothTooltips(left, right) {
        this.overlay.tooltipLeft.update.apply(
            this.overlay.tooltipLeft,
            this.getTooltipUpdate(left)
        )
        
        this.overlay.tooltipRight.update.apply(
            this.overlay.tooltipRight,
            this.getTooltipUpdate(right)
        )
    }
}