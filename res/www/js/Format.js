const __MONTH_NAME__ = [
    'Jan.',
    'Feb.',
    'Mar.',
    'Apr.',
    'May.',
    'Jun.',
    'Jul.',
    'Aug.',
    'Sep.',
    'Oct.',
    'Nov.',
    'Dec.',
]

function formatDate(date) {
    return `${__MONTH_NAME__[date.getMonth()]} ${date.getDate()}, ${date.getFullYear()}`
}

function formatDateRange(dateA, dateB) {
    const dur = moment.duration(moment(dateB).diff(dateA)) 

    if (dur.asDays() < 1)
        return "less than 1 day"

    if (dur.asMonths() < 1)
        return `${dur.days()} days`

    if (dur.asYears() < 1)
        return `${dur.months()} months, ${dur.days()} days`

    return `${dur.years()} years, ${dur.months()} months, ${dur.days()} days`
}

function formatWeight(weight) {
    return weight.toFixed(1)
}

function formatWeightRange(numRecords) {
    return `${numRecords} records`
}

function formatStat(stat) {
    return stat.toFixed(3)
}

function formatPercentChange(percent) {
    if (percent < 0)
        return `${(-percent * 100).toFixed(1)}% increase`
    return `${(percent * 100).toFixed(1)}% reduction`
}